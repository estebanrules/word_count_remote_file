# Count the occurrences of a word in a remote file using the open-uri library.

require 'open-uri'
word_count = open("http://ruby-metaprogramming.rubylearning.com/html/ruby_metaprogramming_1.html").read.scan(/the/).count
puts word_count
